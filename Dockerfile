FROM ubuntu

RUN apt-get update
RUN apt-get install -y python

ADD * /home/linc2functionpipeline/

CMD tail -f /dev/null
# CMD ["/home/linc2functionpipeline/hello.py"]

ENTRYPOINT ["python"]
