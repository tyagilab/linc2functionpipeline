import os
import pickle
from tensorflow import keras
import Identify

class HsModel:

    def __init__(self, mode, modelRoot, scalerRoot):
        if (mode == 'basic'):
            modelPath = os.path.join(modelRoot, 'ann_1_model_hs.h5')
            scalerRoot = os.path.join(scalerRoot, 'scaler_1_hs.pkl')
        elif (mode == 'standard'):
            modelPath = os.path.join(modelRoot, 'ann_2_model_hs.h5')
            scalerRoot = os.path.join(scalerRoot, 'scaler_2_hs.pkl')
        self.model = keras.models.load_model(modelPath)
        self.scaler = pickle.load(open(scalerRoot,'rb'))
        self.mode = mode

    def predict(self, seq):
        if self.mode == 'basic':
            hs_features = Identify.extractHSFeaturesForBasicModel(seq)
        elif self.mode == 'standard':
            hs_features = Identify.extractHSFeaturesForStandardModel(seq)
        hs_features_scaled = self.scaler.transform([list(map(float, hs_features))])
        hs_preds = self.model.predict([hs_features_scaled])
        return hs_preds

    def predict_classes(self, seq):
        if self.mode == 'basic':
            hs_features = Identify.extractHSFeaturesForBasicModel(seq)
        elif self.mode == 'standard':
            hs_features = Identify.extractHSFeaturesForStandardModel(seq)
        hs_features_scaled = self.scaler.transform([list(map(float, hs_features))])
        hs_preds_classes = self.model.predict_classes([hs_features_scaled])
        return hs_preds_classes
