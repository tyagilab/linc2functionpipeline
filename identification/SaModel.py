import os
import pickle
from tensorflow import keras
import Identify

class SaModel:

    def __init__(self, mode, modelRoot, scalerRoot):
        if (mode == 'basic'):
            modelPath = os.path.join(modelRoot, 'ann_1_model_sa.h5')
            scalerRoot = os.path.join(scalerRoot, 'scaler_1_sa.pkl')
        elif (mode == 'standard'):
            modelPath = os.path.join(modelRoot, 'ann_2_model_sa.h5')
            scalerRoot = os.path.join(scalerRoot, 'scaler_2_sa.pkl')
        self.model = keras.models.load_model(modelPath)
        self.scaler = pickle.load(open(scalerRoot,'rb'))
        self.mode = mode

    def predict(self, seq):
        if self.mode == 'basic':
            sa_features = Identify.extractSAFeaturesForBasicModel(seq)
        elif self.mode == 'standard':
            sa_features = Identify.extractSAFeaturesForStandardModel(seq)
        sa_features_scaled = self.scaler.transform([list(map(float, sa_features))])
        sa_preds = self.model.predict([sa_features_scaled])
        return sa_preds
