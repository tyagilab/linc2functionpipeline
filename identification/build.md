# Build function user guide

## To display help menu

### Command

`python identification/build.py --help`

### Output

```{shell}
usage: build.py [-h] [-msp MODEL_SOURCE_PATH]
                coding_source_path non_coding_source_path no_of_epochs
                scaler_save_path model_save_path

EHRQC

positional arguments:
  coding_source_path    Path of the fasta file containing the coding
                        transcripts
  non_coding_source_path
                        Path of the fasta file containing the non-coding
                        transcripts
  no_of_epochs          Number of epochs to train the model
  scaler_save_path      Path to save the built scalers
  model_save_path       Path to save the built models

optional arguments:
  -h, --help            show this help message and exit
  -msp MODEL_SOURCE_PATH, --model_source_path MODEL_SOURCE_PATH
                        Path of the model to be updated
```


If the model source path is provided, the utility will update the model (transfer learning). If in case it is not provided, the utility will create the model from the scratch.

## Example to create the model from scratch

`python identification/build.py /home/yramakri/lz25/minor_thesis_yash/workspace/linc2functionpipeline/data/gencode.v34.pc_transcripts.random.fa /home/yramakri/lz25/minor_thesis_yash/workspace/linc2functionpipeline/data/LncBook_LNCipedia_GENCODE_NONCODE.fa 100 /home/yramakri/lz25/minor_thesis_yash/workspace/linc2functionpipeline/temp/temp_scaler.pkl /home/yramakri/lz25/minor_thesis_yash/workspace/linc2functionpipeline/temp/temp_model.h5`

## Example to update the model (perform transfer learning)

`python identification/build.py /home/yramakri/lz25/minor_thesis_yash/workspace/linc2functionpipeline/data/gencode.v34.pc_transcripts.random.fa /home/yramakri/lz25/minor_thesis_yash/workspace/linc2functionpipeline/data/LncBook_LNCipedia_GENCODE_NONCODE.fa 100 /home/yramakri/lz25/minor_thesis_yash/workspace/linc2functionpipeline/temp/temp_scaler.pkl /home/yramakri/lz25/minor_thesis_yash/workspace/linc2functionpipeline/temp/temp_model.h5 -msp=/home/yramakri/lz25/minor_thesis_yash/workspace/linc2functionpipeline/temp/temp_model.h5`
