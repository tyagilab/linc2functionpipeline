import numpy as np
import Identify
from Bio import SeqIO
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler, LabelEncoder

from tensorflow import keras

from keras.models import Sequential
from keras.layers import Dense

import pickle

import argparse


TEMP_DIR = '/home/yramakri/lz25/minor_thesis_yash/workspace/linc2functionpipeline/temp/'
FASTA_DIR = '/home/yramakri/lz25/minor_thesis_yash/workspace/linc2functionpipeline/data/'
CODING_FILE = FASTA_DIR + 'gencode.v34.pc_transcripts.random.fa'
NONCODING_FILE = FASTA_DIR + 'LncBook_LNCipedia_GENCODE_NONCODE.fa'
MODEL_PATH = TEMP_DIR + 'temp_model.h5'
SCALER_PATH = TEMP_DIR + 'temp_scaler.pkl'


def buildSABModel(codingSourcePath, nonCodingSourcePath, noOfEpochs, modelSavePath, scalerSavePath, modelSourcePath):

    print('Start')
    X = []
    y = []

    print('Reading the non coding file')
    for record in SeqIO.parse(codingSourcePath, "fasta"):
        sequence = str(record.seq)
        sabm_features = Identify.extractSAFeaturesForBasicModel(sequence)
        X.append(sabm_features)
        y.append('noncoding')

    print('Reading the coding file')
    for record in SeqIO.parse(nonCodingSourcePath, "fasta"):
        sequence = str(record.seq)
        sabm_features = Identify.extractSAFeaturesForBasicModel(sequence)
        X.append(sabm_features)
        y.append('coding')

    X = np.asarray(X, dtype=np.float32)
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)
    scaler = StandardScaler()
    scaler.fit(X)
    X_scaled = scaler.transform(X)
    X_train_scaled = scaler.transform(X_train)
    X_test_scaled = scaler.transform(X_test)
    y_encoded = LabelEncoder().fit_transform(y)
    y_train_encoded = LabelEncoder().fit_transform(y_train)
    y_test_encoded = LabelEncoder().fit_transform(y_test)

    if modelSourcePath:
        model = keras.models.load_model(modelSourcePath)
    else:
        input_len = X_train_scaled.shape[1]
        input_len_by_two = int(input_len/2)
        model = Sequential()
        model.add(Dense(input_len_by_two, input_dim=input_len, activation='relu'))
        model.add(Dense(input_len_by_two, activation='relu'))
        model.add(Dense(1, activation='sigmoid'))
        # compile the keras model
        model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])

    model.fit(X_train_scaled, y_train_encoded, epochs=noOfEpochs, batch_size=256, validation_data=(X_test_scaled, y_test_encoded))

    model.save(modelSavePath)
    pickle.dump(scaler, open(scalerSavePath,'wb'))


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='EHRQC')

    parser.add_argument('coding_source_path', help='Path of the fasta file containing the coding transcripts')

    parser.add_argument('non_coding_source_path', help='Path of the fasta file containing the non-coding transcripts')

    parser.add_argument('no_of_epochs', help='Number of epochs to train the model')

    parser.add_argument('scaler_save_path', help='Path to save the built scalers')

    parser.add_argument('model_save_path', help='Path to save the built models')

    parser.add_argument('-msp', '--model_source_path', help='Path of the model to be updated')

    args = parser.parse_args()

    print('args.coding_source_path: ' + str(args.coding_source_path))
    print('args.non_coding_source_path: ' + str(args.non_coding_source_path))
    print('args.no_of_epochs: ' + str(args.no_of_epochs))
    print('args.scaler_save_path: ' + str(args.scaler_save_path))
    print('args.model_save_path: ' + str(args.model_save_path))
    print('args.model_source_path: ' + str(args.model_source_path))

    buildSABModel(
        codingSourcePath = str(args.coding_source_path),
        nonCodingSourcePath = str(args.non_coding_source_path),
        noOfEpochs = int(args.no_of_epochs),
        modelSavePath = str(args.model_save_path),
        scalerSavePath = str(args.scaler_save_path),
        modelSourcePath = args.model_source_path,
        )
