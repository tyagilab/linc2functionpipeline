# from Bio.SeqUtils.CodonUsage import CodonAdaptationIndex
import math

class CodonBias:

    codonBiasIndex = {
        "TTT": 0.46,
        "TCT": 0.19,
        "TAT": 0.44,
        "TGT": 0.46,
        "TTC": 0.54,
        "TCC": 0.22,
        "TAC": 0.56,
        "TGC": 0.54,
        "TTA": 0.08,
        "TCA": 0.15,
        "TAA": 0.3,
        "TGA": 0.47,
        "TTG": 0.13,
        "TCG": 0.05,
        "TAG": 0.24,
        "TGG": 1,
        "CTT": 0.13,
        "CCT": 0.29,
        "CAT": 0.42,
        "CGT": 0.08,
        "CTC": 0.2,
        "CCC": 0.32,
        "CAC": 0.58,
        "CGC": 0.18,
        "CTA": 0.07,
        "CCA": 0.28,
        "CAA": 0.27,
        "CGA": 0.11,
        "CTG": 0.4,
        "CCG": 0.11,
        "CAG": 0.73,
        "CGG": 0.2,
        "ATT": 0.36,
        "ACT": 0.25,
        "AAT": 0.47,
        "AGT": 0.15,
        "ATC": 0.47,
        "ACC": 0.36,
        "AAC": 0.53,
        "AGC": 0.24,
        "ATA": 0.17,
        "ACA": 0.28,
        "AAA": 0.43,
        "AGA": 0.21,
        "ATG": 1,
        "ACG": 0.11,
        "AAG": 0.57,
        "AGG": 0.21,
        "GTT": 0.18,
        "GCT": 0.27,
        "GAT": 0.46,
        "GGT": 0.16,
        "GTC": 0.24,
        "GCC": 0.4,
        "GAC": 0.54,
        "GGC": 0.34,
        "GTA": 0.12,
        "GCA": 0.23,
        "GAA": 0.42,
        "GGA": 0.25,
        "GTG": 0.46,
        "GCG": 0.11,
        "GAG": 0.58,
        "GGG": 0.25,
    }

    # def __init__(self):
        # self.CAI = CodonAdaptationIndex()
        # self.CAI.set_cai_index(self.codonBiasIndex)

    # def printIndex(self):
    #     return self.CAI.print_index()

    def calculateCai(self, seq):
        cai_value, cai_length = 0, 0
        if seq.islower():
            seq = seq.upper()

        for i in range(0, len(seq), 3):
            codon = seq[i : i + 3]
            if codon in self.codonBiasIndex:
                # these two codons are always one, exclude them:
                if codon not in ["ATG", "TGG"]:
                    cai_value += math.log(self.codonBiasIndex[codon])
                    cai_length += 1

        if cai_length == 0:
            return 0
        elif cai_length == 1:
            return math.exp(cai_value)

        return math.exp(cai_value / (cai_length - 1.0))

if __name__ == "__main__":
    seq = 'GAG'
    codonBias = CodonBias()
    print(codonBias.calculateCai(seq))
