import requests
import numpy as np

from utils import GetFastaFromText

def getExonCountLength(gencodeId):
    POST_URL = 'http://www.ensembl.org/biomart/martservice?query=<?xml version="1.0" encoding="UTF-8"?><!DOCTYPE Query><Query  virtualSchemaName = "default" formatter = "FASTA" header = "0" uniqueRows = "0" count = "" datasetConfigVersion = "0.6" ><Dataset name = "hsapiens_gene_ensembl" interface = "default" ><Filter name = "ensembl_transcript_id_version" value = "' + gencodeId + '"/><Attribute name = "ensembl_transcript_id_version" /><Attribute name = "gene_exon" /></Dataset></Query>'
    response = requests.get(POST_URL)
    [seqID, seqlist] = GetFastaFromText(response.text)
    exonCount = len(seqID)
    exonLength = [len(seq) for seq in seqlist]
    return exonCount, np.mean(exonLength)
