import os

from . import FeatureExtractor
from .HmmFea import RunHMM
from .HmmFea import ReadHmm
from .HmmFea import GenerateTrans
from .Hexamer import HexamerScore2
from .Hexamer import ReadLogScore
from utils import GetFasta


class FeatureConverter:

    TMPDIR = '/home/monash/minor_thesis/workspace/linc2function/tmp/'
    DATADIR = '/home/monash/minor_thesis/workspace/linc2function/data/'
    PFAM = '/home/monash/minor_thesis/workspace/linc2function/data/Pfam-A.hmm'
    HMM_THREADS = 8

    def __init__(self, fastaFile):

        self.fastaFile = fastaFile
        # translate fasta to protein and HMMER
        tmp_protein_file = os.path.join(
            FeatureConverter.TMPDIR, "test_protein.fasta"
        )
        GenerateTrans(fastaFile, tmp_protein_file)
        tmp_hmmer_prefix = os.path.join(FeatureConverter.TMPDIR, "test_hmmer")
        tmp_hmmer_out = os.path.join(
            FeatureConverter.TMPDIR, "test_hmmer.out2"
        )
        RunHMM(tmp_protein_file, tmp_hmmer_prefix, FeatureConverter.PFAM,
               thread=FeatureConverter.HMM_THREADS)

        self.HMMdict1 = ReadHmm(tmp_hmmer_out)
        os.remove(tmp_protein_file)
        os.remove(tmp_hmmer_out)

        log_hexamer = os.path.join(
            FeatureConverter.DATADIR, "human.hexamer.logscore"
        )
        self.logscore_dict = ReadLogScore(log_hexamer)

    def convert(self):
        SeqID, SeqList = GetFasta(self.fastaFile)
        features = []
        for i in range(len(SeqID)):
            print(('# DEBUG: i:', i))
            seq = SeqList[i]
            seq = seq.strip()
            seqId = SeqID[i]
            # HMMER index
            if seqId in self.HMMdict1:
                hmmfeature = self.HMMdict1[seqId][1] + "\t" + \
                    self.HMMdict1[seqId][2] + "\t" + self.HMMdict1[seqId][3]
            else:
                hmmfeature = "0\t0\t0"

            # search ORF
            ORF, UTR5, UTR3 = FeatureExtractor.GetORF_UTR(seq)
            transcript_len = len(seq)

            # ORF features
            ORF_fea = str(
                len(ORF)) + "\t" + str(len(ORF) * 1.0 / transcript_len
                                       )

            # The EDP of ORF
            if len(ORF) < 6:
                EDP_fea = FeatureExtractor.GetEDP_noORF()
            else:
                EDP_fea = FeatureExtractor.GetEDP(ORF, transcript_len)

            Kmer_EDP_fea = FeatureExtractor.GetKmerEDP(ORF)

            # UTR feature
            UTR5_fea = str(len(UTR5) * 1.0 / len(seq)) + "\t" + \
                str(FeatureExtractor.GetGC_Content(UTR5))
            UTR3_fea = str(len(UTR3) * 1.0 / len(seq)) + "\t" + \
                str(FeatureExtractor.GetGC_Content(UTR3))

            # Hexamer feature
            ave_hexamer_score = HexamerScore2(ORF, self.logscore_dict)
            hex_score = str(ave_hexamer_score)

            # Fickett feature
            A_pos_fea = FeatureExtractor.GetBasePositionScore(seq, 'A')
            C_pos_fea = FeatureExtractor.GetBasePositionScore(seq, 'C')
            G_pos_fea = FeatureExtractor.GetBasePositionScore(seq, 'G')
            T_pos_fea = FeatureExtractor.GetBasePositionScore(seq, 'T')
            base_ratio = FeatureExtractor.GetBaseRatio(seq)

            fickett_fea = "\t".join(
                [str(A_pos_fea), str(C_pos_fea), str(G_pos_fea),
                    str(T_pos_fea), str(base_ratio[0]), str(base_ratio[1]),
                 str(base_ratio[2]), str(base_ratio[3])]
            )

            rbpFeatures = FeatureExtractor.getRBPFeatures(seq)

            feature = "\t".join([EDP_fea, ORF_fea, hmmfeature,
                                 Kmer_EDP_fea, UTR5_fea, UTR3_fea, hex_score,
                                 fickett_fea, rbpFeatures])
            features.append(seqId + '<>' + feature)

        return features
