import subprocess
import pandas as pd
from io import StringIO

DATA_DIR = '/home/monash/minor_thesis/workspace/linc2function/data/'
FASTA_DIR = DATA_DIR + 'fasta/'
FASTA_FILE = FASTA_DIR + '34_v15_ncrnas.fasta'

def getIntaRNAFeatures(seq):
    command = ['IntaRNA', '--outMode', 'C', '--threads', '0', '-t', FASTA_FILE, '-q', seq]
    output = subprocess.run(command, stdout=subprocess.PIPE).stdout.decode('utf-8')
    intaRNADf = pd.read_csv(StringIO(output), header=0, sep=';')
    return min(intaRNADf['E']), max(intaRNADf['E']), sum(intaRNADf['E'])


if __name__ == '__main__':
    MIN_IE, MAX_IE, TOTAL_IE = getIntaRNAFeatures('AAAACCCCCCCUUUU')
    print(MIN_IE, MAX_IE, TOTAL_IE)
