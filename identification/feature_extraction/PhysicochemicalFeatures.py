import numpy as np

def getPhysicochemicalFeatures(seq):
    numericSeq = []
    for s in seq:
        if s == 'A':
            numericSeq.append(0.1260)
        elif s == 'C':
            numericSeq.append(0.1340)
        elif s == 'G':
            numericSeq.append(0.0806)
        elif s == 'T':
            numericSeq.append(0.1335)
        else:
            numericSeq.append(0)
    fftSeq = np.fft.fft(numericSeq)
    seqSpectrum = np.square(np.abs(fftSeq))
    lenSeqSpectrum = len(seqSpectrum)
    k = int(lenSeqSpectrum/3)
    DftMax = max(seqSpectrum[k-3], seqSpectrum[k-2], seqSpectrum[k-1], seqSpectrum[k], seqSpectrum[k+1])
    totalPower  = sum(seqSpectrum) / lenSeqSpectrum
    snr = DftMax/totalPower
    seqSpectrumSorted = np.sort(seqSpectrum)
    qnt = np.quantile(seqSpectrumSorted, q = [0.25, 0.5, 0.75])
    return DftMax, snr, qnt[0], qnt[1], qnt[2]
