from bs4 import BeautifulSoup
import requests
# from utils import GetFasta

POST_URL = '''http://rbpdb.ccbr.utoronto.ca/cgi-bin/sequence_scan.pl'''
RBP_NAMES = ["RBM5", "RBM6", "YBX2", "CSDE1", "PTBP1", "ZC3H3", "MATR3",
             "SAMD4A", "YTHDC2", "CUGBP2", "PUM2", "RC3H2", "ZC3H11A",
             "PARP12", "CSDA", "SFRS8", "EIF4B", "U2AF2", "SFRS14", "SPEN",
             "ZC3H15", "YBX1", "ELAVL1", "THUMPD1", "DHX8", "SRBD1",
             "PABPC1", "DAZAP1", "IGF2BP2", "ZNF638", "SART3", "MKRN2",
             "RBM7", "RBMS2", "MBNL3", "SNRPA", "SYNJ2", "A2BP1", "DDX43",
             "KIAA0020", "CNOT4", "YTHDC1", "PPIE", "CHERP", "RBM22", "KHSRP",
             "FUS", "RBM41", "PCBP4", "PABPC4", "TNRC6A", "RBM27", "HNRNPC",
             "DAZL", "HNRNPH3", "SETD1A", "CIRBP", "HNRNPM", "TRMT2A", "SF3A1",
             "SNRPD3", "POLDIP3", "ZMAT5", "RBM9", "ZC3H7B", "RBM23", "SFRS5",
             "ZC3H14", "ACIN1", "PABPN1", "PABPC1L", "BRUNOL4", "CSTF2",
             "ZC3H12B", "FMR1", "HTATSF1", "RBM3", "ESRP2", "MTHFSD",
             "DNAJC17", "MYEF2", "ESRP1", "HNRNPL", "SNRNP70", "SFRS16",
             "TRMT1", "NOVA2", "SF4", "ZC3HAV1", "EIF3B", "RBM28", "LSM5",
             "EIF4H", "ELAVL2", "FUBP3", "CPEB3", "LARP4B", "SUPT6H",
             "PPARGC1A", "CPSF6", "KRR1", "SFRS9", "SFRS3", "RBM24",
             "KHDRBS2", "QKI", "CPEB4", "FXR1", "NCL", "HDLBP", "SFRS7",
             "PNO1", "TIA1", "SFRS4", "SFPQ", "SFRS11", "PRPF3", "PTBP2",
             "ROD1", "RBM18", "C14orf156", "RBM25", "ENOX1", "TARDBP", "AKAP1",
             "PSPC1", "ZCCHC17", "KHDRBS1", "ZC3H7A", "HNRNPA2B1", "BICC1",
             "RBM19", "ZC3H13", "SFRS6", "RNF113A", "SNRPD2", "SNRPB",
             "SNRPB2", "HNRNPR", "RALY", "RBM42", "HNRNPH2", "ZFP36", "NAA38",
             "SNRPN", "FXR2", "SAFB2", "LSM7", "LSM4", "ZC3H4", "EIF3G",
             "DKC1", "PPIL4", "RBM39", "ANKHD1", "KHDRBS3", "RBM8A", "LIN28",
             "GRSF1", "ANKRD17", "UNK", "NIP7", "TOE1", "RBM38", "SRRM1",
             "MKRN1", "EIF2S1", "THUMPD3", "RBMX2", "PUM1", "MSI1", "SYNCRIP",
             "ZC3H10", "HNRNPA1", "RC3H1", "IGF2BP3", "NUPL2", "SFRS1",
             "TRA2B", "CPEB2", "ALKBH8", "SLTM", "PNPT1", "THUMPD2", "ASCC1",
             "SSB", "HNRNPD", "LARP1B", "G3BP2", "ZCRB1", "SNRPF", "SETD1B",
             "RBM26", "MBNL2", "RNF113B", "NOVA1", "BRUNOL6", "DUS3L", "SF3B4",
             "LGTN", "HNRPLL", "SNRPG", "ZC3H8", "RBMS3", "G3BP1", "NONO",
             "RBMX", "A1CF", "PPRC1", "PDCD11", "TUT1", "CUGBP1", "RPS3",
             "ZC3H12C", "CPSF7", "YTHDF1", "PABPC3", "TIAL1", "RBM46", "UHMK1",
             "BOLL", "ZFP36L2", "PAN3", "MBNL1", "HNRPDL", "CARHSP1", "RBMS1",
             "SFRS12", "MSI2", "SFRS13B", "C4orf23", "MKI67IP", "LARP1",
             "RBM45", "PPARGC1B", "LSM11", "SFRS15", "RBPMS", "ZC3H18",
             "SYNJ1", "IGF2BP1", "TNRC4", "U2AF1", "SAFB", "CPSF4", "BRUNOL5",
             "U2AF1L4", "SFRS2", "LARP4", "RAVER1", "ELAVL4", "RAVER2",
             "FUBP1", "RBM15", "DHX57", "TDRD10", "TIPARP", "RBM47", "SR140",
             "TTC14", "LSM6", "TRA2A", "HNRNPK", "ENOX2", "LARP6", "KIAA0430",
             "RBPMS2", "SNRPD1", "SAMD14", "POLR2G", "SF1", "HNRNPH1", "ZRSR2",
             "PCBP1", "RBMY1F", "HNRNPF", "HNRNPA3", "RBMXL2", "LSM3",
             "NCBP2L", "CSDC2", "TAF15", "RBM4B", "RBM4", "LARP7", "PABPC5",
             "LSM1", "RBMXL3", "MEX3C", "RBM44", "CSTF2T", "HNRNPA0",
             "ZC3H12D", "SAMD4B", "HNRNPCL1", "MKRN3", "RBM15B", "PUF60",
             "TRNAU1AP", "SFRS2B", "MEX3D", "LSM10", "SNRPE", "TDRKH", "RBM10",
             "LENG9", "EWSR1", "LSMD1", "DND1", "MEX3B", "PCBP3", "THOC4",
             "RBM12B", "SNRNP35", "PABPC1L2B", "RALYL", "DDX53", "RBM33",
             "RBM43", "RBM11", "ZFP36L1", "YTHDF3", "RNPC3", "PABPC1L2A",
             "DAZ3", "RDM1", "LIN28B", "CPSF4L", "DAZ1", "ZC3H6", "SFRS13A",
             "RBM34", "RRP7A", "ELAVL3", "C14orf21", "PCBP2", "ZGPAT",
             "HNRNPAB", "NOL8", "HELZ", "YTHDF2", "SFMBT2", "L3MBTL3", "MEX3A",
             "RBM20", "DPPA5", "MIR1236", "PPP1R10", "PRR3", "PABPN1L",
             "RNPS1", "DAZ2", "ZRSR1", "RBM16", "RBMXL1", "CPEB1", "RBMY1J",
             "SNRPEL1", "MCTS1", "PABPC4L", "RBMY1A1", "MKRNP5", "RBM14",
             "NSUN6", "UNKL", "RBMY1E", "HNRNPA1L2", "RBMY1B", "RBMY1D",
             "RBM12", "DAZ4", "DGKQ", "RBM17", "MCM3AP", "PARN", "TLR2",
             "PARP10", "MOV10L1", "CCAR1", "LEMD3", "UPF3B", "RCAN2", "SRRT",
             "ZC3H12A", "ZC3HAV1L", "PARP14", "TMEM63A", "SLBP", "GTF3A",
             "NUFIP1", "SRRM2", "APTX", "ZFR", "UPF1", "KIN", "ZNF239",
             "ZNF74", "SNRPC", "C1orf25", "ZFR2", "IREB2", "ACO1", "TROVE2",
             "TEP1", "GAPDH", "ZRANB2"]


def getRBPFeatures(sequence, threshold):

    RBPScores = [0] * len(RBP_NAMES)
    RBPRelativeScores = [0] * len(RBP_NAMES)
    RBPRelativeCounts = [0] * len(RBP_NAMES)
    postParams = {'seq': sequence, 'thresh': threshold}
    postOutput = requests.post(POST_URL, data=postParams)
    soup = BeautifulSoup(postOutput.text, features="lxml")
    table = soup.find('table', {'class', 'pme-main'})

    if table:
        for row in table.findAll('tr'):
            col = row.findAll('td')
            if col:
                try:
                    index = RBP_NAMES.index(col[2].string.strip())
                    RBPScores[index] = col[0].string.strip()
                    RBPRelativeScores[index] = float(
                        col[1].string.strip().rstrip("%")
                    )
                    RBPRelativeCounts[index] = RBPRelativeCounts[index] + 1
                except ValueError:
                    pass
    RBPFeatures = RBPScores + RBPRelativeScores + RBPRelativeCounts
    RBPFeatures = '\t'.join(map(str, RBPFeatures))
    return RBPFeatures


def getRBPTotalScore(sequence, threshold):

    RBPScores = []
    postParams = {'seq': sequence, 'thresh': threshold}
    postOutput = requests.post(POST_URL, data=postParams)
    soup = BeautifulSoup(postOutput.text, features="lxml")
    table = soup.find('table', {'class', 'pme-main'})

    if table:
        for row in table.findAll('tr'):
            col = row.findAll('td')
            if col:
                try:
                    RBPScores.append(float(col[0].string.strip()))
                except:
                    pass
    return sum(RBPScores)


if __name__ == '__main__':
    # dataDir = '/home/monash/minor_thesis/workspace/linc2function/data/'
    # fastaFile = dataDir + 'lncRNA_mRNA_test_1.fa'
    # SeqID, SeqList = GetFasta(fastaFile)
    # for seq in SeqList:
    #     print(('# DEBUG: RBPFeatures:', getRBPFeatures(seq, 0.8)))

    # getRBPFeatures(
    #     'GAUCGCUGAACCCGAAGGGGCGGGGGACCCAGGGGGCGAAUCUCUUCCGAAAGGAAGAGUAGGGUUACUCCUUCGACCCGAGCCCGUCAGCUAACCUCGCAAGCGUCCGAAGGAGAAAA', 
    #     0.8)


    totalScore = getRBPTotalScore(
        'GAUCGCUGAACCCGAAGGGGCGGGGGACCCAGGGGGCGAAUCUCUUCCGAAAGGAAGAGUAGGGUUACUCCUUCGACCCGAGCCCGUCAGCUAACCUCGCAAGCGUCCGAAGGAGAAAA', 
        0.8)
    print(totalScore)


    # postParams = {'seq': 'GAUCGCUGAACCCGAAGGGGCGGGGGACCCAGGGGGCGAAUCUCUUCCGAAAGGAAGAGUAGGGUUACUCCUUCGACCCGAGCCCGUCAGCUAACCUCGCAAGCGUCCGAAGGAGAAAA', 'thresh': 0.8}
    # postOutput = requests.post(POST_URL, data=postParams)
    # soup = BeautifulSoup(postOutput.text, features="lxml")
    # table = soup.find('table', {'class', 'pme-main'})

    # headers = []
    # data = []
    # if table:
    #     i = 0
    #     for row in table.findAll('tr'):
    #         if i == 0:
    #             headers_row = row.findAll('th')
    #             if headers_row:
    #                 headers = [header.string for header in headers_row][:6]
    #         else:
    #             data_row = row.findAll('td')
    #             if data_row:
    #                 data.append([cell.string for cell in data_row][:6])
    #         i += 1

    # print('headers: ')
    # print(headers)
    # print('data: ')
    # print(data)
