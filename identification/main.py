import os, sys
import pickle
import pandas as pd
import numpy as np
import Identify
from tensorflow import keras
from Bio import SeqIO
from HsModel import HsModel
from SaModel import SaModel


def main():
    FASTA_DIR = '/home/monash/minor_thesis/workspace/linc2function/data/fasta/'
    CODING_FILE = FASTA_DIR + 'gencode.v34.pc_transcripts.random.fa'
    NONCODING_FILE = FASTA_DIR + 'LncBook_LNCipedia_GENCODE_NONCODE.fa'

    hs_column_names = ['ORF_LENGTH', 'UTR5_GC_CONTENT', 'HEXAMER_BIAS', 'CODON_BIAS', 'FICKETT_A_POS', 'FICKETT_G_POS', 'FICKETT_T_POS', 'DFT_MAX', 'SNR', 'QNT_3']
    sa_column_names = ['ORF_LENGTH', 'UTR5_GC_CONTENT', 'FICKETT_A_POS', 'FICKETT_G_POS', 'FICKETT_T_POS', 'FICKETT_BASE_RATIO_2', 'FICKETT_BASE_RATIO_4', 'SNR', 'QNT_2', 'QNT_3']

    hs_features_list = []
    sa_features_list = []
    for record in SeqIO.parse(NONCODING_FILE, "fasta"):
        sequence = str(record.seq)
        hs_features = Identify.extractHSFeatures(sequence)
        sa_features = Identify.extractSAFeatures(sequence)
        hs_features_list.append(hs_features)
        sa_features_list.append(sa_features)

    hs_df = pd.DataFrame(hs_features_list, columns = hs_features)
    sa_df = pd.DataFrame(sa_features_list, columns = sa_features)

    hs_scaler = pickle.load(open('identification/scalers/scaler_hs.pkl','rb'))
    sa_scaler = pickle.load(open('identification/scalers/scaler_sa.pkl','rb'))

    hs_df_scaled = hs_scaler.transform(hs_df.astype(np.float64))
    sa_df_scaled = sa_scaler.transform(sa_df.astype(np.float64))

    hs_model = keras.models.load_model('identification/models/ann_1_model_hs.h5')
    sa_model = keras.models.load_model('identification/models/ann_1_model_sa.h5')

    hs_preds = hs_model.predict(hs_df_scaled)
    sa_preds = sa_model.predict(sa_df_scaled)

    print(hs_preds[:4])
    print(sa_preds[:4])

def predict_hs_model(seq, mode, modelRoot, scalerRoot):
    hs_model = HsModel(mode, modelRoot, scalerRoot)
    preds = hs_model.predict(seq)
    print(preds)
    return(preds)


def predict_sa_model(seq, mode, modelRoot, scalerRoot):
    sa_model = SaModel(mode, modelRoot, scalerRoot)
    preds = sa_model.predict(seq)
    print(preds)
    return(preds)


if __name__ == '__main__':
    # main()
    # preds_hs = predict_hs_model('GATCGCTGAACCCGAAGGGGCGGGGGACCCAGGGGGCGAATCTCTTCCGAAAGGAAGAGTAGGGTTACTCCTTCGACCCGAGCCCGTCAGCTAACCTCGCAAGCGTCCGAAGGAGAAAA', 'basic', 'identification/models', 'identification/scalers')
    # preds_hs = predict_hs_model('GATCGCTGAACCCGAAGGGGCGGGGGACCCAGGGGGCGAATCTCTTCCGAAAGGAAGAGTAGGGTTACTCCTTCGACCCGAGCCCGTCAGCTAACCTCGCAAGCGTCCGAAGGAGAAAA', 'standard', 'identification/models', 'identification/scalers')
    # preds_sa = predict_sa_model('GAUCGCUGAACCCGAAGGGGCGGGGGACCCAGGGGGCGAAUCUCUUCCGAAAGGAAGAGUAGGGUUACUCCUUCGACCCGAGCCCGUCAGCUAACCUCGCAAGCGUCCGAAGGAGAAAA', 'basic', 'identification/models', 'identification/scalers')
    # print(preds_hs)
    # print(preds_sa)
    # seq = 'GAUCGCUGAACCCGAAGGGGCGGGGGACCCAGGGGGCGAAUCUCUUCCGAAAGGAAGAGUAGGGUUACUCCUUCGACCCGAGCCCGUCAGCUAACCUCGCAAGCGUCCGAAGGAGAAAA'
    # seq = seq.replace('U', 'T')
    # globals()['predict_sa_model'](seq, 'basic', 'identification/models', 'identification/scalers')
    # globals()['predict_sa_model'](seq, 'standard', 'identification/models', 'identification/scalers')
    # print(preds_hs)
    # print(preds_sa)
    seq = sys.argv[2]
    seq = seq.replace('U', 'T')
    globals()[sys.argv[1]](seq, sys.argv[3], sys.argv[4], sys.argv[5])
