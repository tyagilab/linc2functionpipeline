from Bio import SeqIO

# for record in SeqIO.parse("example.fasta", "fasta"):
#     print(record.id)


import pandas as pd

dataDf = pd.read_csv('/home/yram0006/phd/chapter_1/workspace/linc2functionpipeline/identification/data/mouse/mouse_benchmark.csv')

records = []
for record in SeqIO.parse("/home/yram0006/phd/chapter_1/workspace/linc2functionpipeline/identification/data/mouse/gencode.vM25.pc_transcripts.fa", "fasta"):
    records.append(record)

outputRecords = []
for id in dataDf.ID:
    for record in records:
        if(record.id.startswith(id)):
            outputRecords.append(record)

with open("/home/yram0006/phd/chapter_1/workspace/linc2functionpipeline/identification/data/mouse/gencode.vM25.pc_transcripts.random.fa", "w") as output_handle:
    SeqIO.write(outputRecords, output_handle, "fasta")
